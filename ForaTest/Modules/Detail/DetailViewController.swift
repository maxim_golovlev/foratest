//
//  DetailViewController.swift
//  ForaTest
//
//  Created by Admin on 27.10.17.
//  Copyright (c) 2017 Admin. All rights reserved.
//

import UIKit
import TableKit
import SDWebImage
import AVKit
import AVFoundation

protocol DetailViewProtocol: class, BaseView {
    func dataDidUploaded(songs: [Song])
    func trackDownloaded(localPath: URL)
}

class DetailViewController: UIViewController {
  
  // MARK: - Public properties
    
    var album: Album?
  
  lazy var presenter:DetailPresenterProtocol = DetailPresenter(view: self)
  
  // MARK: - Private properties
    @IBOutlet weak var albumTitleLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            self.tableDirector = TableDirector.init(tableView: tableView)
            tableView.tableFooterView = UIView()
        }
    }
    
    var tableDirector: TableDirector!
    
    // MARK: - View lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    albumTitleLabel.text = album?.title
    artistLabel.text = album?.artist
    genreLabel.text = album?.genre
    
    if let str = album?.artworkUrl, let url = URL.init(string: str) {
        self.iconView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "music-placeholder"))
    }
    
    presenter.fetchSongs(albumId: album?.id)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  // MARK: - Display logic
  
  // MARK: - Actions
    
  // MARK: - Overrides
    
  // MARK: - Private functions
    
    func string(milisec: Int?) -> String? {
        var time: String?
        
        if let mSec = milisec {
            
            let interval = TimeInterval.init(mSec / 1000)
            let calendar = Calendar.init(identifier: .gregorian)
            let units = Set<Calendar.Component>([.minute, .second])
            let components = calendar.dateComponents(units, from: Date(), to: Date.init(timeIntervalSinceNow: interval))
            
            time = String.init(format: "%02i : %02i", components.minute ?? 0, components.second ?? 0)
        }
        return time
    }
    
    func playDownload(_ url: URL) {
        let playerViewController = AVPlayerViewController()
        present(playerViewController, animated: true, completion: nil)
        let player = AVPlayer(url: url)
        playerViewController.player = player
        player.play()
    }
}

extension DetailViewController:  DetailViewProtocol {
    func dataDidUploaded(songs: [Song]) {
        
        tableDirector.clear()
        
        let section = TableSection()
        
        for song in songs {

            let row = TableRow<SongCell>.init(item: (title: song.title, duration: self.string(milisec: song.duration))).on(.click) { [unowned self] options in
                
                guard let url = song.url else { return }
                
                let localState = iTunesManager.shared.localFileExists(for: url)
                
                if localState.0 {
                    self.playDownload(localState.1)
                } else {
                    self.presenter.dowloadTrack(url: url)
                }
            }
            section.append(row: row)
        }
        
        tableDirector.append(section: section)
        
        tableDirector.reload()
    }
    
    func trackDownloaded(localPath: URL) {
        playDownload(localPath)
    }
}
