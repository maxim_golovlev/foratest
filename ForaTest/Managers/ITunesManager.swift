//
//  ITunesManager.swift
//  ForaTest
//
//  Created by Admin on 27.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import PromiseKit

class iTunesManager: MainApi{
    
    private init() { }
    
    static let shared = iTunesManager()
    
    struct APIKeys {
        static func album(s: String) -> String? {
            guard let s = s.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else { return nil }
            return "https://itunes.apple.com/search?term=\(s)&media=music&entity=album"
        }
        
        static func song(id: Int) -> String? {
            return "https://itunes.apple.com/lookup?id=\(id)&entity=song"
        }
    }
    
    func fetchAlbums(query: String) -> Promise<[Album]> {
        
        return Promise.init(resolvers: { (fullfill, reject) in
            
            let url = APIKeys.album(s: query)
            
            sendRequest(withUrl: url) { (response) in
                
                switch response {
                case let .Success(response: json):
                    if let array = json["results"] as? [[String: Any]] {
                        let albums = array.flatMap { Album.initfromDict(dict: $0) }.filter({ $0.title != nil }) .sorted(by: { $0.title! < $1.title! })
                        fullfill(albums)
                    } else {
                        reject(ResponseError.withMessage("error trying to convert data to JSON"))
                    }
                case let .Error(message: msg):
                    reject(ResponseError.withMessage(msg))
                }
            }
        })
    }
    
    func fetchSongs(albumId: Int) -> Promise<[Song]> {
        
        return Promise.init(resolvers: { (fullfill, reject) in
            
            let url = APIKeys.song(id: albumId)
            
            sendRequest(withUrl: url) { (response) in
                
                switch response {
                case let .Success(response: json):
                    if let array = json["results"] as? [[String: Any]] {
                        let song = array.dropFirst().flatMap { Song.initfromDict(dict: $0) }
                        fullfill(song)
                    } else {
                        reject(ResponseError.withMessage("error trying to convert data to JSON"))
                    }
                case let .Error(message: msg):
                    reject(ResponseError.withMessage(msg))
                }
            }
        })
    }
    
    func downloadSong(url: URL) -> Promise<URL> {
        
        return Promise.init(resolvers: { (fullfill, reject) in
            
            download(url: url, completion: { (response) in
                switch response {
                case let .Success(response: url):
                    fullfill(url)
                case let .Error(message: msg):
                    reject(ResponseError.withMessage(msg))
                }
            })
        })
    }
}
