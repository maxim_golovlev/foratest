//
//  FeedPresenter.swift
//  ForaTest
//
//  Created by Admin on 27.10.17.
//  Copyright (c) 2017 Admin. All rights reserved.
//

import UIKit

protocol FeedPresenterProtocol: class {
    weak var view:FeedViewProtocol? { get set }
    func fetchAlbums(title: String?)
}

class FeedPresenter {
  
  // MARK: - Public variables
  weak var view:FeedViewProtocol?
  
  // MARK: - Private variables
  
  // MARK: - Initialization
  init(view:FeedViewProtocol) {
    self.view = view
  }
    
    func fetchAlbums(title: String?) {
        
        guard let title = title else { return }
        
        if title.characters.count <= 0 { return }
        
        view?.startLoading()
        
        iTunesManager.shared.fetchAlbums(query: title)
            .then { (albums) -> Void in
                self.view?.dataDidUploaded(albums: albums)
            }
            .always {
                self.view?.stopLoading()
            }
            .catch { (error) in
                if case let ResponseError.withMessage(msg) = error {
                    self.view?.showAlert(title: nil, message: msg)
                }
        }
    }
}

extension FeedPresenter: FeedPresenterProtocol {
  
}
