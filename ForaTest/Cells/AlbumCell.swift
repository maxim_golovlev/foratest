//
//  AlbumCell.swift
//  ForaTest
//
//  Created by Admin on 27.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import SDWebImage
import TableKit

class AlbumCell: UITableViewCell, ConfigurableCell {

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var ganreLabel: UILabel!
    
    static var defaultHeight: CGFloat? {
        return 150
    }
    
    func configure(with data:(title: String?, artist: String?, ganre: String?, icon: String?)) {
        
        self.titleLabel.text = data.title
        self.artistLabel.text = data.artist
        self.ganreLabel.text = data.ganre
        
        if let str = data.icon, let url = URL.init(string: str) {
            self.icon.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "music-placeholder"))
        }
    }
}
