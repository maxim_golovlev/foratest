//
//  MainAPI.swift
//  ForaTest
//
//  Created by Admin on 27.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

protocol MainApi {
    
    func sendRequest(withUrl url: String?, completion: ServerResult?)
    
}

extension MainApi {
    
    func sendRequest(withUrl url: String?, completion: ServerResult?) {
        
        guard let str = url, let url = URL.init(string: str)
            else {
                completion?(.Error(message: "Incorrect url"))
                return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, responce, error) in
            
            do {
                guard let data = data, let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] else {
                    completion?(.Error(message: "error trying to convert data to JSON"))
                    return
                }
                completion?(.Success(response: json))
            } catch  {
                completion?(.Error(message: "error trying to convert data to JSON"))
                return
            }
            }.resume()
        
    }
    
    func download(url sourceURL: URL, completion: ServerDownloadResult?) {
        
        URLSession.shared.downloadTask(with: sourceURL) { (tempLocalUrl, response, error) in
            
            if let error = error {
                completion?(.Error(message: error.localizedDescription))
                return
            }
            
            guard let tempLocalUrl = tempLocalUrl else {
                completion?(.Error(message: "error during track download"))
                return
            }
            
            let destinationURL = self.localFilePath(for: sourceURL)
            
            let fileManager = FileManager.default
            try? fileManager.removeItem(at: destinationURL)
            do {
                try fileManager.copyItem(at: tempLocalUrl, to: destinationURL)
                completion?(.Success(response: destinationURL))
            } catch let error {
                completion?(.Error(message:"Could not copy file to disk: \(error.localizedDescription)"))
            }
            
        }.resume()
    }
    
    func localFileExists(for url: URL) -> (Bool, URL) {
        let localUrl = localFilePath(for: url)
        var isDir: ObjCBool = false
        let state = FileManager.default.fileExists(atPath: localUrl.path, isDirectory: &isDir)
        return (state, localUrl)
    }
    
    func localFilePath(for url: URL) -> URL {
        return documentsPath.appendingPathComponent(url.lastPathComponent)
    }
    
    var documentsPath:URL {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    }
}
