//
//  SongCell.swift
//  ForaTest
//
//  Created by Admin on 28.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

class SongCell: UITableViewCell, ConfigurableCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    
    static var defaultHeight: CGFloat? {
        return 44
    }
    
    func configure(with data:(title: String?, duration: String?)) {
        
        self.titleLabel.text = data.title
        self.durationLabel.text = data.duration
    }
}
