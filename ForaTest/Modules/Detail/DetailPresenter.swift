//
//  DetailPresenter.swift
//  ForaTest
//
//  Created by Admin on 27.10.17.
//  Copyright (c) 2017 Admin. All rights reserved.
//

import UIKit

protocol DetailPresenterProtocol: class {
    weak var view:DetailViewProtocol? { get set }
    func fetchSongs(albumId: Int?)
    func dowloadTrack(url: URL?)
}

class DetailPresenter {
  
  // MARK: - Public variables
  weak var view:DetailViewProtocol?
  
  // MARK: - Private variables
  
  // MARK: - Initialization
  init(view:DetailViewProtocol) {
    self.view = view
  }
    
    func fetchSongs(albumId: Int?) {
        
        guard let id = albumId else { return }
        
        view?.startLoading()
        
        iTunesManager.shared.fetchSongs(albumId: id)
            .then { (songs) -> Void in
                self.view?.dataDidUploaded(songs: songs)
            }
            .always {
                self.view?.stopLoading()
            }
            .catch { (error) in
                if case let ResponseError.withMessage(msg) = error {
                    self.view?.showAlert(title: nil, message: msg)
                }
        }
    }
    
    func dowloadTrack(url: URL?) {
        
        guard let url = url else { return }
        
            self.view?.startLoading()
            iTunesManager.shared.downloadSong(url: url)
                .then { (url) -> Void in
                    self.view?.trackDownloaded(localPath: url)
                }
                .always {
                    self.view?.stopLoading()
                }
                .catch { (error) in
                    if case let ResponseError.withMessage(msg) = error {
                        self.view?.showAlert(title: nil, message: msg)
                    }
        }
    }
}

extension DetailPresenter: DetailPresenterProtocol {
  
    
}
