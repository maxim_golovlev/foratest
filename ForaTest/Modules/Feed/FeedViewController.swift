//
//  FeedViewController.swift
//  ForaTest
//
//  Created by Admin on 27.10.17.
//  Copyright (c) 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

protocol FeedViewProtocol: class, BaseView {
  func dataDidUploaded(albums: [Album])
}

class FeedViewController: UIViewController {
  
  // MARK: - Public properties
  
  lazy var presenter:FeedPresenterProtocol = FeedPresenter(view: self)
  
    @IBOutlet weak var tableView: UITableView!
    // MARK: - Private properties
    
    lazy var searchVC: UISearchController = {
        let vc = UISearchController.init(searchResultsController: self.tableViewController)
        vc.searchResultsUpdater = self

        self.definesPresentationContext = true
        return vc
    }()
    
    lazy var tableViewController: UITableViewController = {
        let vc = UITableViewController.init()
        self.tableDirector = TableDirector.init(tableView: vc.tableView)
        if #available(iOS 10.0, *) {
            vc.automaticallyAdjustsScrollViewInsets = false
            vc.tableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0)
            vc.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(64, 0, 0, 0)
        }
        return vc
    }()
    
    var tableDirector: TableDirector!
  
  // MARK: - View lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    navigationController?.navigationBar.isTranslucent = false
    
    tableView.tableHeaderView = searchVC.searchBar
    tableView.tableFooterView = UIView()

  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  // MARK: - Display logic
  
  // MARK: - Actions
  
  // MARK: - Overrides
    
  // MARK: - Private functions
}

extension FeedViewController:  FeedViewProtocol {
    func dataDidUploaded(albums: [Album]) {
        
        tableDirector.clear()
        
        let section = TableSection()
        
        for album in albums {
            
            let albumRow = TableRow<AlbumCell>.init(item: (title: album.title, artist: album.artist, ganre: album.genre, icon: album.artworkUrl)).on(.click) { [unowned self] options in
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                if let vc = storyboard.instantiateViewController(withIdentifier :"DetailViewController") as? DetailViewController {
                    
                    vc.album = album
                    vc.title = "Playlist"
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
            section.append(row: albumRow)
        }
        
        tableDirector.append(section: section)
        
        tableDirector.reload()
    }
}

extension FeedViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        presenter.fetchAlbums(title: searchController.searchBar.text)
    }
    
}
