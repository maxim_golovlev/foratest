//
//  Album.swift
//  ForaTest
//
//  Created by Admin on 27.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

struct Album {
    
    let title: String?
    let artist: String?
    let genre: String?
    let artworkUrl: String?
    let id: Int?
    
    static func initfromDict(dict: [String: Any]?) -> Album? {
        
        guard let dict = dict,
            let title = dict["collectionName"] as? String,
            let artist = dict["artistName"] as? String,
            let genre = dict["primaryGenreName"] as? String,
            let artworkUrl = dict["artworkUrl100"] as? String,
            let id = dict["collectionId"] as? Int else { return nil }
        
        let album = Album.init(title: title, artist: artist, genre: genre, artworkUrl: artworkUrl, id: id)
        return album
    }
    
}
