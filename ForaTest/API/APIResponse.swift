import Foundation

enum APIDownloadResponse {
    case Success (response: URL)
    case Error (message: String?)
}

typealias ServerDownloadResult = (_ response: APIDownloadResponse) -> Void

enum APIResponse {
    case Success (response: Dictionary<String, Any>)
    case Error (message: String?)
}

typealias ServerResult = (_ response: APIResponse) -> Void

enum APIArrayResponse {
    case Success (response: [Dictionary<String, Any>])
    case Error (message: String?)
}

typealias ServerArrayResult = (_ response: APIArrayResponse) -> Void

enum ResponseError: Error {
    case withMessage(String?)
}
