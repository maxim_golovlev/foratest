//
//  Song.swift
//  ForaTest
//
//  Created by Admin on 27.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

struct Song {
    
    let title: String?
    let duration: Int?
    let url: URL?
    
    static func initfromDict(dict: [String: Any]?) -> Song? {
        
        guard let dict = dict,
            let title = dict["trackName"] as? String,
            let duration = dict["trackTimeMillis"] as? Int ,
            let url = dict["previewUrl"] as? String else { return nil }
        
        let song = Song.init(title: title, duration: duration, url: URL.init(string: url) )
        return song
    }
}
